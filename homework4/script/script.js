// ================THEORY======================================
/*
1. Що таке цикл в програмуванні?
Цикл - це блок коду, завдання якого здійснити повторення самого себе до визначеного ним моменту зупинки.

2. Які види циклів є в JavaScript і які їх ключові слова?

В JS є наступні види циклів: 

- for;
- while;
- do / while;

3. Чим відрізняється цикл do while від while?

Цикл (do while) завжди виконує оператор, хоча б один раз на відмінну від циклу (while), який при негативному першому колі циклу зупиняє його і в результаті нічого не виведе.

do while нам необхідний тоді коли нам потрібно обов'язков вивести в консоль якесь значення.
*/



// ================PRACTICE====================================

// 1.

let numOne = parseInt(prompt("Введіть будьласка перше число"));
let numTwo = parseInt(prompt("Введіть будьласка друге число"));

while (numOne !== Number(numOne)){
    numOne = parseInt(prompt("Введіть будьласка перше число"));
}
while (numTwo !== Number(numTwo)){
    numTwo = parseInt(prompt("Введіть будьласка друге число"));
}

if(numOne <= numTwo){
    for(let i = numOne; i <= numTwo; i++){
        console.log(i);
    }
}else{
    for(let i = numTwo; i <= numOne; i++){
        console.log(i);
    }
}

// 2.

let even = parseInt(prompt("Введіть парне число"));

while(even % 2 !== 0){
    even = parseInt(prompt("Введіть парне число"));
}
console.log(even);